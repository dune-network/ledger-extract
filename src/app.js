const extract = require('./index.js').extract


var extract_ed25519_btn = document.getElementById('extract_ed25519');
var extract_secp256k1_btn = document.getElementById('extract_secp256k1');
var mnemonic = document.getElementById('mnemonic');
var password = document.getElementById('password');
var path = document.getElementById('path');

var pkh = document.getElementById('pkh');
var pkht = document.getElementById('pkht');
var pk = document.getElementById('pk');
var sk = document.getElementById('sk');
var esk = document.getElementById('esk');

var error_div = document.getElementById('error');
var result_div = document.getElementById('result');
var warn_online = document.getElementById('warn-online');

function extract_and_populate(curve) {
    sk.value = "";
    pk.value = "";
    esk.value = "";
    pkh.value = "";
    pkht.value = "";
    error_div.style = "display:none";
    result_div.style = "display:none";

    mnemonic.classList.remove("is-valid");
    mnemonic.classList.remove("is-invalid");
    path.classList.remove("is-valid");
    path.classList.remove("is-invalid");

    try {
        let x = extract(mnemonic.value, path.value, curve, password.value);
        mnemonic.classList.add("is-valid");
        path.classList.add("is-valid");
        result_div.style = "";
        sk.value = x.base58.secretKey;
        pk.value = x.base58.publicKey;
        esk.value = x.base58.extendedSecretKey;
        pkh.value = x.base58.publicKeyHash.dune;
        pkht.value = x.base58.publicKeyHash.tezos;
    } catch (e) {
        console.error(e);
        error_div.style = "";
        error_div.innerText = e;
        if (e instanceof Error) {
            if (e.message == "Invalid mnemonic") {
                mnemonic.classList.add("is-invalid");
            }
            if (e.message == "Invalid derivation path" ||
                e.message.startsWith("Expected BIP32Path")) {
                path.classList.add("is-invalid");
            }
        }
    }
}

extract_ed25519_btn.onclick = () => { extract_and_populate("ed25519") };
extract_secp256k1_btn.onclick = () => { extract_and_populate("secp256k1") };
