"use strict";

const bip39 = require('bip39')
const bip32 = require('bip32')
const bs58check = require('bs58check')
const nacl = require('tweetnacl')
const blake = require('blakejs')
const hd = require('./hd.js')

const dn1_prefix = new Uint8Array([4, 177, 1]);
const dn2_prefix = new Uint8Array([4, 177, 3]);
const dn3_prefix = new Uint8Array([4, 177, 6]);

const tz1_prefix = new Uint8Array([6, 161, 159]);
const tz2_prefix = new Uint8Array([6, 161, 161]);
const tz3_prefix = new Uint8Array([6, 161, 164]);

const edpk_prefix = new Uint8Array([13, 15, 37, 217]);
const sppk_prefix = new Uint8Array([3, 254, 226, 86]);
const p2pk_prefix = new Uint8Array([3, 178, 139, 127]);

const edsk_long_prefix = new Uint8Array([43, 246, 78, 7]);
const edsk_prefix = new Uint8Array([13, 15, 58, 7]);
const edesk_prefix = new Uint8Array([7, 90, 60, 179, 41]);
const spsk_prefix = new Uint8Array([17, 162, 224, 201]);
const p2sk_prefix = new Uint8Array([16,81,238,189]);

function uint8concat(a, b) {
    var c = new Uint8Array(a.length + b.length);
    c.set(a);
    c.set(b, a.length);
    return c;
}

function b58(prefix, k) {
    return bs58check.encode(uint8concat(prefix, k));
}

function ed25519_derivePath (seed, path) {
    const child = new Uint8Array(hd.derivePath(path, seed).key);
    const key_pair = nacl.sign.keyPair.fromSeed(child);
    return { secretKey: child,
             keyPair: key_pair };
}

function secp256k1_derivePath (seed, path) {
    const node = bip32.fromSeed(seed);
    const child = node.derivePath(path);
    const sk = new Uint8Array(child.privateKey);
    const pk = new Uint8Array(child.publicKey);
    return {
        secretKey: sk,
        keyPair: {
            secretKey: sk,
            publicKey: pk
        }
    }
}

function extract(input_mnemonic, path, curve, password) {

    switch (curve) {
    case 'ed25519':
    case undefined:
        var derivePath = ed25519_derivePath;
        var dn_prefix = dn1_prefix;
        var tz_prefix = tz1_prefix;
        var pk_prefix = edpk_prefix;
        var sk_prefix = edsk_prefix;
        var sk_long_prefix = edsk_long_prefix;
        break;
    case 'secp256k1':
        var derivePath = secp256k1_derivePath;
        var dn_prefix = dn2_prefix;
        var tz_prefix = tz2_prefix;
        var pk_prefix = sppk_prefix;
        var sk_prefix = spsk_prefix;
        var sk_long_prefix = spsk_prefix;
        break;
    default:
        throw(new Error("Unknown curve"))
    }

    const mnemonic = input_mnemonic.trim().split(' ').filter(x => x).join(' ');
    if (!bip39.validateMnemonic(mnemonic)) {
        throw(new Error("Invalid mnemonic"))
    }
    const seed = bip39.mnemonicToSeedSync(mnemonic, password);

    const key = derivePath(seed, path);

    const sk = key.keyPair.secretKey;
    const pk = key.keyPair.publicKey;
    const pkh = blake.blake2b(pk, null, 20);

    const key_b58 = b58(sk_prefix, key.secretKey);
    const sk_b58 = b58(sk_long_prefix, sk);
    const pk_b58 = b58(pk_prefix, pk);
    const pkh_b58_dune = b58(dn_prefix, pkh);
    const pkh_b58_tezos = b58(tz_prefix, pkh);

    key.publicKeyHash = pkh;
    key.base58 = {
        secretKey: key_b58,
        extendedSecretKey: sk_b58,
        publicKey: pk_b58,
        publicKeyHash : {
            dune: pkh_b58_dune,
            tezos: pkh_b58_tezos
        }
    };

    return key;
}

exports.extract = extract
